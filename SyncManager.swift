//
//  SyncManager.swift
//  Support-App
//
//  Created by Charles Eric Vincent on 2017-02-06.
//  Copyright © 2017 Vincent Solutions. All rights reserved.
//

import UIKit
import TwilioSyncClient
import Alamofire
import SwiftyJSON
import Sync
import CoreData

class SyncManager: NSObject {
    static let sharedManager = SyncManager()
    fileprivate override init() {}
    
    var syncClient: TwilioSyncClient?
    
    func login() {
        if self.syncClient != nil {
            logout()
        }
        let token = generateToken()
        let properties = TwilioSyncClientProperties()
        if let token = token {
            self.syncClient = TwilioSyncClient(token: token, properties: properties, delegate: self)
        }
    }
    
    func logout() {
        if let syncClient = syncClient {
            syncClient.shutdown()
            self.syncClient = nil
        }
    }
    
    fileprivate func generateToken() -> String? {
        let email = "test@test.com"
        let identifierForVendor = UIDevice.current.identifierForVendor?.uuidString
        let urlString = "https://platform.vincentsolutions.ca/api/sync/auth?device=\(identifierForVendor!)&email=\(email)"
        var token : String?
        do {
            if let url = URL(string: urlString),
                let data = try? Data(contentsOf: url),
                let result = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] {
                token = result["token"] as? String
            }
        } catch {
            print("Error obtaining token: \(error)")
        }
        return token
    }
}

extension SyncManager: TwilioSyncClientDelegate {
    
}
