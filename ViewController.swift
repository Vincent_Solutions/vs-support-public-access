//
//  ProtectedVC.swift
//  Support-App
//
//  Created by Vincent Solutions on 2016-10-23.
//  Copyright © 2016 Vincent Solutions. All rights reserved.
//

import UIKit
import CoreData
import Sync
import SYNCPropertyMapper
import DATAStack
import Alamofire
import SwiftyJSON
import JWTDecode
import TwilioSyncClient

class ProtectedVC: UIViewController, UIScrollViewDelegate {
    
    let dataStack = DATAStack = DATAStack(modelName: "UserData")
    
    @IBOutlet weak var userFullName: UILabel!
    @IBOutlet weak var userBalance: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /***
         Work flow of the UIViewController :
         1 - Upon a successful login, a downloadDocumentV2() is invoked before showing this UIViewController
         2 - When it gets to viewDidAppear, fetchMetaObjectsFromDBV2() is invoked to get the items from CoreData that were previously synced from a successful invocation of downloadDocumentV2().
         3 - Upon receiving the items, an attempt is made to retrieve each meta from the items.
         4 - If an item is retrieved successfully, the corresponding UILabel is populated with the meta.
         ***/
        
        /***
         Work flow of an incoming document :
         1 - Each time a document update is done by the REST API, onDocument(remoteUpdated) should be invoked.
         2 - Which, in turn, should invoke a new syncDocumentWithCoreDataV2() to sync the new document data with CoreData
         3 - Which, in turn, should invoke a fetchMetaObjectsFromDBV2() so that the next time a viewDidAppear is invoked, the UILabels gets updated.
        ***/
        
        self.fetchMetaObjectsFromDBV2() { items in
            for item in items {
                if let firstName = item.value(forKey: "firstName") as? String {
                    if let lastName = item.value(forKey: "lastName") as? String {
                        self.userFullName.text = "\(firstName) \(lastName)"
                        self.userFullName.isHidden = false
                    }
                }
                if let creditBalance = item.value(forKey: "mycredDefault") as? String {
                    self.userBalance.text = "\(creditBalance) min"
                    self.userBalance.isHidden = false
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Downloads the Sync Document
    func downloadDocumentV2(callback: @escaping ([NSManagedObject]) -> Void) {
        let email = KeychainWrapper.standard.string(forKey: "email")
        if let syncClient = SyncManager.sharedManager.syncClient {
            if let options = TWSOpenOptions.withUniqueName(email!) {
                syncClient.openDocument(with: options, delegate: self, completion: { (result, document) in
                    if !(result?.isSuccessful())! {
                        print("TTT: error creating document: \(result?.error)")
                    } else {
                        self.syncDocumentWithCoreDataV2(document: document!) { items in
                            callback(items)
                        }
                    }
                })
            }
        }
    }
    
    // Syncs the Sync document with Core Data using SyncDB & DataStack libraries
    func syncDocumentWithCoreDataV2(document: TWSDocument, callback: @escaping ([NSManagedObject]) -> Void) {
        let data = document.getData()
        Sync.changes([data], inEntityNamed: "Usermeta", dataStack: self.dataStack) { error in
            
            if let error = error {
                print("Sync error : \(error.localizedDescription)")
            }
            
            if error == nil {
                self.fetchMetaObjectsFromDBV2 { items in
                    callback(items)
                }
            }
        }
    }
    
    // Fetches the user data from Core Data to populate fields
    func fetchMetaObjectsFromDBV2(callback: @escaping ([NSManagedObject]) -> Void) {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Usermeta")
        request.sortDescriptors = [NSSortDescriptor(key: "remoteID", ascending: true)]
        let items = (try! self.dataStack.mainContext.fetch(request)) as! [NSManagedObject]
        callback(items)
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    // MARK: - CoachMarkControllerDelegate

}

extension ProtectedVC: TWSDocumentDelegate {
    func onDocument(_ document: TWSDocument, resultDataUpdated data: [String : Any], forFlowID flowId: UInt) {
        print("Document Update Data")
        self.syncDocumentWithCoreDataV2(document: document) { items in
        }
    }
    
    func onDocument(_ document: TWSDocument, remoteUpdated data: [String : Any]) {
        print("Document remote update")
        self.syncDocumentWithCoreDataV2(document: document) { items in
        }
    }
    
    func onDocument(_ document: TWSDocument, remoteErrorOccurred error: TWSError) {
        print("TTT: document: \(document) error: \(error)")
    }
}
